from flask_restplus import Api
from flask import Blueprint

from .main.controller.user_controller import api as user_ns
from .main.controller.auth_controller import api as auth_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='Template API documentation',
          version='1.0',
          description='A template for flask restplus service',
          )

api.add_namespace(user_ns, path='/users')
api.add_namespace(auth_ns)