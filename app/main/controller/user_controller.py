from flask import request
from flask_restplus import Resource

from app.main.utils.decorator import token_required
from ..utils.dto import UserDto
from ..service.user_service import get_all_users, save_new_user, get_user, delete_user, update_user

api = UserDto.api
_user = UserDto.user

@api.route('/')
class UserList(Resource):
    @api.doc('list_of_registered_users')
    @token_required
    @api.marshal_list_with(_user, envelope='data')
    def get(self):
        """List all registered users"""
        return get_all_users()

    @api.response(201, 'User successfully created.')
    @api.doc('create a new user')
    # @token_required
    @api.expect(_user, validate=True)
    def post(self):
        """Creates a new User """
        data = request.json
        return save_new_user(data=data)


@api.route('/<public_id>')
@api.param('public_id', 'The User identifier')
@api.response(404, 'User not found.')
class User(Resource):
    @api.doc('get user')
    @token_required
    @api.marshal_with(_user)
    def get(self, public_id):
        """Get a user given its identifier"""
        user = get_user(public_id)
        if not user:
            api.abort(404)
        else:
            return user

    @api.doc('delete user')
    @token_required
    @api.marshal_with(_user)
    def delete(self, public_id):
        """Delete a user given its identifier"""
        user = get_user(public_id)
        if not user:
            api.abort(404)
        else:
            return delete_user(public_id)

    @api.doc('update user')
    @token_required
    @api.marshal_with(_user)
    @api.expect(_user, validate=True)
    def put(self, public_id):
        """Update a user given its identifier and data"""
        user = get_user(public_id)
        data = request.json
        if not user:
            api.abort(404)
        else:
            return update_user(public_id, data)