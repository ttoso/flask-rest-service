# Flask API REST template for new projects
## Technology 
- Python 3.7
- Virtual develop environment with Pipenv
- All necesary modules in Pipfile
- Flask Restplus for the API
## Installation
- install pipenv
- cd root_directory
- pipenv shell (This will install al modules and go to the virtual environment)
## MySQL config
- apt install libmysqlclient-dev | Needed to property working of mysql python interface.
## Usage 
- make clean: cleans up the app
- make system-packages: install python, pip and pipenv
- make python-packages: install packages from pipfile
- make install: installs both system-packages and python-packages
- make shell: start the virtual environment
- make db-prepare: generate necessary scripts for db migration
- make db-init: create the database from the existing models
- make tests: runs the all the tests
- make run: starts the application
- make all: performs clean-up, installation, run tests, and starts the app
## Environment
- BOILERPLATE_ENV can have 3 different values: dev, prod and test with different configurations at app/main/config.py. Default value is dev.
  