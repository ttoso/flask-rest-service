.PHONY: clean system-packages python-packages install tests run all

clean:
	find . -type f -name '*.pyc' -delete
	find . -type f -name '*.log' -delete

system-packages:
	sudo apt install python3.7
	sudo apt install python3-pip -y
	sudo apt install libmysqlclient-dev
	sudo -H pip3 install -U pipenv
python-packages:
	pipenv install 

shell:
	pipenv shell

install:
	make system-packages
	make python-packages
	
db-prepare:
	python manage.py db init
	
db-init:
	python manage.py db migrate
	python manage.py db upgrade

tests:
	python manage.py test

run:
	python manage.py run

all:
	make clean
	make install
	make tests 
	make run
